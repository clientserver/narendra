#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

int main(int argc,char *argv[])
{
	int sd=0,conn=0,n=0;
	struct sockaddr_in clnt;
	char recvbuff[1000];

	if(argc!=2)
		{
			printf("\n ip of server:%s",argv[0]);
			return 1;
		}
	memset(recvbuff,'0',sizeof(recvbuff));
	memset(&clnt,'0',sizeof(clnt));
	
	if((sd=socket(AF_INET,SOCK_STREAM,0))<0)
	{
		printf("\nsocket not created");
		return 1;	
	}
	clnt.sin_family=AF_INET;
	clnt.sin_port=htons(4000);
	
	if(inet_pton(AF_INET,argv[1],&clnt.sin_addr)<=0)
	{
		printf("error in assigning ip address");
		return 1;
	}

	if(connect(sd,(struct sockaddr*)&clnt,sizeof(clnt))<0)
	{	
		printf("connection failed");
		return 1;
	}
	printf("\n\nconnection stablished\n");
	while((n=read(sd,recvbuff,sizeof(recvbuff)-1))>0)
	{
		if(fputs(recvbuff,stdout)==EOF)
		{
			printf("\n ERROR in reading buffer");
		}
	}

	if(n<0)
		printf("\n\nread error");
			
	else
		printf("\nmessage from server: %s\n",recvbuff);
}
